//
//  Line.swift
//  TouchTracker
//
//  Created by Joao Cassamano on 4/9/20.
//  Copyright © 2020 Joao Cassamano. All rights reserved.
//

import Foundation
import CoreGraphics

struct Line{
    var begin = CGPoint.zero
    var end = CGPoint.zero
}
